import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class MasterThread extends MyThread
{
	private static final String FOREMAN_WORK_INDICATOR = "CHAPTER";
	
	public MasterThread(UnderbossThread superior)
	{
		super(superior);
		finishedParts = new TreeMap<>();
		allBookParts = new ConcurrentHashMap<>();
	}
	
	@Override
	public void run()
	{
		ForemanThread[] foremen = new ForemanThread[Main.getForemenUnderMaster()];
		for(int i = 0; i < foremen.length; i++)
		{
			foremen[i] = new ForemanThread(this);
			foremen[i].start();
		}
		
		startWorking(FOREMAN_WORK_INDICATOR);
	}
	
	@Override
	protected void returnResults(OccurrenceMap occurrences, Map<Integer, BookPart> currentStates)
	{
		allBookParts.merge(occurrences.getBookPart().getVolumeBook(),
						   occurrences,
						   OccurrenceMap::merge);
		returnStates(currentStates);
	}
	
	@Override
	protected void saveWork(Map<Integer, BookPart> currentStates)
	{
		for(BookPart chapter : currentStates.values())
		{
			String book = chapter.getVolumeBook();
			
			if(finishedParts.get(book).decrementAndGet() == 0)
			{
				OccurrenceMap occurrences = allBookParts.get(book);
				
				occurrences.save(chapter.getChapterPath(),
								 chapter.getBookName() + SAVE_FILE_SUFFIX);
				
				//save book state
				superior.superior.saveState(chapter.getBookStateName(),
											chapter.getVolumePath());
				superior.saveState(chapter.getBookStateName(),
								   chapter.getBookPath());
				
				superior.returnResults(occurrences, currentStates);
			}
		}
	}
	
	@Override
	protected void addBookPart(Queue<String> bookPartLines, OccurrenceMap previousBookPartMap)
	{
		BookPart bookPart = previousBookPartMap.getBookPart();
		addPartToMap(bookPart.getVolumeBook());
		sendWork(new OccurrenceMap(new BookPart(
				bookPartLines,
				bookPart.getVolume(),
				bookPart.getBook(),
				"" + bookPartNumber++)));
	}
}
