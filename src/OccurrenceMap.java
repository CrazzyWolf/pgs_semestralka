import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListMap;

public class OccurrenceMap
{
	//concurrent skip list map preserves order of keys and it's thread save
	private final ConcurrentSkipListMap<String, Integer> map;
	private BookPart bookPart;
	
	public OccurrenceMap()
	{
		map = new ConcurrentSkipListMap<>();
	}
	
	public OccurrenceMap(BookPart bookPart)
	{
		this();
		this.bookPart = bookPart;
	}
	
	//adds entry and sets it's value to 1 and if it already exist then increment value by 1
	public void add(String string)
	{
		map.merge(string, 1, Integer::sum);
	}
	
	//if map entry does not exist yet then create it else merge it with existing
	public OccurrenceMap merge(OccurrenceMap occurrenceMap)
	{
		occurrenceMap.map.forEach((key, value) -> map.merge(
				key, value, Integer::sum));
		return this;
	}
	
	//saves map to file
	public void save(String folderPath, String fileName)
	{
		ArrayList<String> stringList = new ArrayList<>(map.size() - 1);
		
		map.entrySet()
		   .stream()
		   .skip(1) //first element is sum of all values
		   .forEach(k -> stringList.add(k.getKey() + " " + k.getValue() + "\n"));
		
		Main.save(folderPath, fileName, stringList);
	}
	
	public BookPart getBookPart()
	{
		return bookPart;
	}
}