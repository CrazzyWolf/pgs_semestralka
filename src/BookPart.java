import java.util.Queue;

public class BookPart
{
	private static final String UNDERBOSS_WORK_NAME = "Volume";
	private static final String MASTER_WORK_NAME = "Book";
	private static final String FOREMAN_WORK_NAME = "Chapter";
	private static final String OUTPUT_FOLDER = Main.getOutputFolder().getName();
	
	private final Queue<String> bookPartLines;
	
	private final String volume;
	private final String volumeName;
	private final String volumeNameLC;
	
	private String book;
	private String bookName;
	private String bookNameLC;
	
	private String chapter;
	private String chapterName;
	
	public BookPart(Queue<String> bookPartLines, String volume)
	{
		this.bookPartLines = bookPartLines;
		this.volume = volume;
		volumeName = UNDERBOSS_WORK_NAME + volume;
		volumeNameLC = volumeName.toLowerCase();
	}
	
	public BookPart(Queue<String> bookPartLines, String volume, String book)
	{
		this(bookPartLines, volume);
		this.book = book;
		bookName = MASTER_WORK_NAME + book;
		bookNameLC = bookName.toLowerCase();
	}
	
	public BookPart(Queue<String> bookPartLines, String volume, String book, String chapter)
	{
		this(bookPartLines, volume, book);
		this.chapter = chapter;
		chapterName = FOREMAN_WORK_NAME + chapter;
	}
	
	public Queue<String> getBookPartLines()
	{
		return bookPartLines;
	}
	
	public String getChapter()
	{
		return chapter;
	}
	
	public String getBook()
	{
		return book;
	}
	
	public String getVolume()
	{
		return volume;
	}
	
	public String getVolumeBook()
	{
		return volume + "-" + book;
	}
	
	public String getVolumeBookChapter()
	{
		return volume + "-" + book + "-" + chapter;
	}
	
	public String getVolumePath()
	{
		return OUTPUT_FOLDER + "/";
	}
	
	public String getBookPath()
	{
		return OUTPUT_FOLDER + "/" + volumeNameLC + "/";
	}
	
	public String getChapterPath()
	{
		return OUTPUT_FOLDER + "/" + volumeNameLC + "/" + bookNameLC + "/";
	}
	
	public String getVolumeStateName()
	{
		return UNDERBOSS_WORK_NAME + " " + volume + " - OK\n";
	}
	
	public String getBookStateName()
	{
		return UNDERBOSS_WORK_NAME + " " + volume + " - "
				+ MASTER_WORK_NAME + " " + book + " - OK\n";
	}
	
	public String getChapterStateName()
	{
		return UNDERBOSS_WORK_NAME + " " + volume + " - "
				+ MASTER_WORK_NAME + " " + book + " - "
				+ FOREMAN_WORK_NAME + " " + chapter + " - OK\n";
	}
	
	public String getVolumeName()
	{
		return volumeName;
	}
	
	public String getBookName()
	{
		return bookName;
	}
	
	public String getChapterName()
	{
		return chapterName;
	}
}
