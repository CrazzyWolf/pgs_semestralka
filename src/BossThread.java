import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class BossThread extends MyThread
{
	private static final String UNDERBOSS_WORK_INDICATOR = "VOLUME";
	private static final String BOSS_SAVE_NAME = Main.getOutputFolder() + SAVE_FILE_SUFFIX;
	
	private final OccurrenceMap allOccurrencesList;
	
	public BossThread()
	{
		super(null);
		allOccurrencesList = new OccurrenceMap();
	}
	
	@Override
	public void run()
	{
		//long start = System.nanoTime();
		
		UnderbossThread[] underbosses = new UnderbossThread[Main.getUnderbossesUnderBoss()];
		for(int i = 0; i < underbosses.length; i++)
		{
			underbosses[i] = new UnderbossThread(this);
			underbosses[i].start();
		}
		
		startWorking(UNDERBOSS_WORK_INDICATOR);
		
		allOccurrencesList.save(Main.getOutputFolder().getName(), BOSS_SAVE_NAME);
		
		//System.out.println((System.nanoTime() - start) / 1000000);
	}
	
	@Override
	protected void returnResults(OccurrenceMap occurrences, Map<Integer, BookPart> currentStates)
	{
		allOccurrencesList.merge(occurrences);
		returnStates(currentStates);
	}
	
	@Override
	protected void saveWork(Map<Integer, BookPart> currentStates) { }
	
	@Override
	protected void distributeWork(String workIndicator)
	{
		String line;
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new FileReader(Main.getInputFile()));
			while((line = reader.readLine()) != null)
			{
				if(line.startsWith(workIndicator))
				{
					break;
				}
			}
			
			bookPartNumber = 1;
			Queue<String> volume = new LinkedList<>();
			while((line = reader.readLine()) != null)
			{
				if(line.startsWith(workIndicator))
				{
					addBookPart(new LinkedList<>(volume), null);
					volume.clear();
				}
				else
				{
					volume.add(line);
				}
			}
			addBookPart(volume, null);
			reader.close();
			reader = null;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		if(reader != null)
		{
			try
			{
				reader.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void addBookPart(Queue<String> bookPartLines, OccurrenceMap previousBookPartMap)
	{
		sendWork(new OccurrenceMap(new BookPart(
				bookPartLines,
				"" + bookPartNumber++)));
	}
}