import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class UnderbossThread extends MyThread
{
	private static final String MASTER_WORK_INDICATOR = "BOOK";
	
	public UnderbossThread(BossThread superior)
	{
		super(superior);
		finishedParts = new TreeMap<>();
		allBookParts = new ConcurrentHashMap<>();
	}
	
	@Override
	public void run()
	{
		MasterThread[] masters = new MasterThread[Main.getMastersUnderUnderboss()];
		for(int i = 0; i < masters.length; i++)
		{
			masters[i] = new MasterThread(this);
			masters[i].start();
		}
		
		startWorking(MASTER_WORK_INDICATOR);
	}
	
	@Override
	protected void returnResults(OccurrenceMap occurrences, Map<Integer, BookPart> currentStates)
	{
		allBookParts.merge(occurrences.getBookPart().getVolume(),
						   occurrences,
						   OccurrenceMap::merge);
		returnStates(currentStates);
	}
	
	@Override
	protected void saveWork(Map<Integer, BookPart> currentStates)
	{
		for(BookPart book : currentStates.values())
		{
			String volume = book.getVolume();
			
			if(finishedParts.get(volume).decrementAndGet() == 0)
			{
				OccurrenceMap occurrences = allBookParts.get(volume);
				
				occurrences.save(book.getBookPath(),
								 book.getVolumeName() + SAVE_FILE_SUFFIX);
				
				//save volume state
				superior.saveState(book.getVolumeStateName(),
								   book.getVolumePath());
				
				superior.returnResults(occurrences, currentStates);
			}
		}
	}
	
	@Override
	protected void addBookPart(Queue<String> bookPartLines, OccurrenceMap previousBookPartMap)
	{
		String volume = previousBookPartMap.getBookPart().getVolume();
		addPartToMap(volume);
		sendWork(new OccurrenceMap(new BookPart(
				bookPartLines,
				volume,
				"" + bookPartNumber++)));
	}
}
