import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

public class Main
{
	private static File outputFolder;
	private static File inputFile;
	private static int underbossesUnderBoss;
	private static int mastersUnderUnderboss;
	private static int foremenUnderMaster;
	private static int slavesUnderForeman;
	
	public static void main(String[] args)
	{
		if(args.length == 6)
		{
			if(Files.exists(Paths.get(args[0])))
			{
				outputFolder = new File(args[1]);
				if(outputFolder.exists() && outputFolder.length() > 0)
				{
					System.out.println("Output folder is not empty"); //program always appends text
				}
				else
				{
					try
					{
						underbossesUnderBoss = Integer.parseInt(args[2]);
						mastersUnderUnderboss = Integer.parseInt(args[3]);
						foremenUnderMaster = Integer.parseInt(args[4]);
						slavesUnderForeman = Integer.parseInt(args[5]);
					}
					catch(NumberFormatException e)
					{
						System.out.println("Invalid number of threads");
						return;
					}
					
					inputFile = new File(args[0]);
					new BossThread().start();
				}
			}
			else
			{
				System.out.println("Input file does not exist");
			}
		}
		else
		{
			System.out.println("Enter all arguments in this order:\n[i] [o] [u] [m] [f] [s]" +
									   "\nwhere\n[i] is input file,\n" +
									   "[o] is output file,\n" +
									   "[u] is number of Underboss threads,\n" +
									   "[m] is number of Master threads,\n" +
									   "[f] is number of Foreman threads,\n" +
									   "[s] is number of Slave threads.");
		}
	}
	
	//saves all lines in list
	public static void save(String folderPath, String fileName, Collection<String> stringList)
	{
		BufferedWriter writer = null;
		try
		{
			Files.createDirectories(Paths.get(folderPath));
			writer = new BufferedWriter(new FileWriter(
					folderPath + "/" + fileName, true));
			for(String line : stringList)
			{
				writer.write(line);
			}
			writer.close();
			writer = null;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		if(writer != null)
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static File getOutputFolder()
	{
		return outputFolder;
	}
	
	public static File getInputFile()
	{
		return inputFile;
	}
	
	public static int getUnderbossesUnderBoss()
	{
		return underbossesUnderBoss;
	}
	
	public static int getMastersUnderUnderboss()
	{
		return mastersUnderUnderboss;
	}
	
	public static int getForemenUnderMaster()
	{
		return foremenUnderMaster;
	}
	
	public static int getSlavesUnderForeman()
	{
		return slavesUnderForeman;
	}
}