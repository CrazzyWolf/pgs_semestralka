import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SlaveThread extends Thread
{
	//for right order of states (shared between all slave threads)
	private static final AtomicInteger STATE_ORDER_NUMBER = new AtomicInteger(0);
	
	private final ForemanThread superior;
	
	public SlaveThread(ForemanThread superior)
	{
		this.superior = superior;
	}
	
	@Override
	public void run()
	{
		OccurrenceMap paragraph;
		while((paragraph = superior.getWork()) != null)
		{
			BookPart bookPart = paragraph.getBookPart();
			for(String line : bookPart.getBookPartLines())
			{
				Arrays.stream(line.toLowerCase().split("\\W")).forEach(paragraph::add);
			}
			
			//linked to preserve insertion order and don't have to be thread safe
			//because it's accessed by one thread at same time
			Map<Integer, BookPart> states = new LinkedHashMap<>();
			states.put(STATE_ORDER_NUMBER.getAndIncrement(), bookPart);
			
			superior.returnResults(paragraph, states);
		}
	}
}