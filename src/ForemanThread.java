import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class ForemanThread extends MyThread
{
	public ForemanThread(MasterThread superior)
	{
		super(superior);
		finishedParts = new TreeMap<>();
		allBookParts = new ConcurrentHashMap<>();
	}
	
	@Override
	public void run()
	{
		SlaveThread[] slaves = new SlaveThread[Main.getSlavesUnderForeman()];
		for(int i = 0; i < slaves.length; i++)
		{
			slaves[i] = new SlaveThread(this);
			slaves[i].start();
		}
		
		startWorking(null);
	}
	
	@Override
	protected void returnResults(OccurrenceMap occurrences, Map<Integer, BookPart> currentStates)
	{
		allBookParts.merge(occurrences.getBookPart().getVolumeBookChapter(),
						   occurrences,
						   OccurrenceMap::merge);
		returnStates(currentStates);
	}
	
	@Override
	protected void saveWork(Map<Integer, BookPart> currentStates)
	{
		for(BookPart paragraph : currentStates.values())
		{
			String chapter = paragraph.getVolumeBookChapter();
			
			if(finishedParts.get(chapter).decrementAndGet() == 0)
			{
				String chapterPath = paragraph.getChapterPath();
				OccurrenceMap occurrences = allBookParts.get(chapter);
				
				occurrences.save(chapterPath,
								 paragraph.getChapterName() + SAVE_FILE_SUFFIX);
				
				//save chapter state
				superior.superior.superior.saveState(paragraph.getChapterStateName(),
													 paragraph.getVolumePath());
				superior.superior.saveState(paragraph.getChapterStateName(),
											paragraph.getBookPath());
				superior.saveState(paragraph.getChapterStateName(),
								   paragraph.getChapterPath());
				
				superior.returnResults(occurrences, currentStates);
			}
		}
	}
	
	@Override
	protected void addBookPart(Queue<String> bookPartLines, OccurrenceMap previousBookPartMap)
	{
		BookPart bookPart = previousBookPartMap.getBookPart();
		addPartToMap(bookPart.getVolumeBookChapter());
		sendWork(new OccurrenceMap(new BookPart(
				bookPartLines,
				bookPart.getVolume(),
				bookPart.getBook(),
				bookPart.getChapter())));
	}
	
	@Override
	protected void distributeWork(String workIndicator)
	{
		OccurrenceMap currentChapter;
		while((currentChapter = superior.getWork()) != null)
		{
			Queue<String> chapter = currentChapter.getBookPart().getBookPartLines();
			Queue<String> paragraph = new LinkedList<>();
			
			for(String bookPartLine : chapter)
			{
				String line = bookPartLine.trim();
				if(line.isEmpty())
				{
					if(!paragraph.isEmpty())
					{
						addBookPart(new LinkedList<>(paragraph), currentChapter);
						paragraph.clear();
					}
				}
				else
				{
					paragraph.add(line);
				}
			}
			if(!paragraph.isEmpty())
			{
				addBookPart(paragraph, currentChapter);
			}
		}
	}
}
