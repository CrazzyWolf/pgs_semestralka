import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class MyThread extends Thread
{
	private static final int TIMEOUT = 10; //timeout of waiting
	
	protected static final String SAVE_FILE_SUFFIX = ".txt";
	protected static final String STATE_FILE = "state" + SAVE_FILE_SUFFIX;
	
	//superior of this thread
	protected final MyThread superior;
	
	//map of finished book parts, when integer drops to zero,
	//it means that all book parts that consists of this book part are done
	protected Map<String, AtomicInteger> finishedParts;
	
	protected int bookPartNumber;
	
	//thread safe variables
	
	protected final AtomicInteger workLeft;
	protected final AtomicBoolean workDone; //false until superior is working
	protected final LinkedBlockingQueue<OccurrenceMap> bookPartsQueue;
	protected final LinkedBlockingQueue<Map<Integer, BookPart>> underlingWorkQueue;
	
	//concurrent map that stores all occurrences of given book part
	protected ConcurrentHashMap<String, OccurrenceMap> allBookParts;
	
	protected MyThread(MyThread superior)
	{
		this.superior = superior;
		
		workLeft = new AtomicInteger(0);
		workDone = new AtomicBoolean(false);
		bookPartsQueue = new LinkedBlockingQueue<>();
		underlingWorkQueue = new LinkedBlockingQueue<>();
	}
	
	//method for underlings to return their results
	protected abstract void returnResults(OccurrenceMap occurrences, Map<Integer, BookPart> currentStates);
	
	//method where this thread saves work collected from underlings
	protected abstract void saveWork(Map<Integer, BookPart> currentStates);
	
	//method for sending work from this thread to it's underlings
	protected abstract void addBookPart(Queue<String> bookPartLines, OccurrenceMap previousBookPartMap);
	
	protected void startWorking(String workIndicator)
	{
		distributeWork( workIndicator);
		workDone.set(true);
		syncUnderling(1);
		collectWork();
	}
	
	//this thread splits work for underlings and save it to underlingWorkQueue
	protected void distributeWork(String workIndicator)
	{
		OccurrenceMap bookPart;
		while((bookPart = superior.getWork()) != null)
		{
			Queue<String> linesList = bookPart.getBookPart().getBookPartLines();
			String s;
			while(!((s = linesList.peek()) == null || s.startsWith(workIndicator)))
			{
				linesList.remove();
			}
			linesList.poll();
			
			bookPartNumber = 1;
			LinkedList<String> bookPartLines = new LinkedList<>();
			for(String line : linesList)
			{
				if(line.startsWith(workIndicator))
				{
					addBookPart(new LinkedList<>(bookPartLines), bookPart);
					bookPartLines.clear();
				}
				else
				{
					bookPartLines.add(line);
				}
			}
			addBookPart(bookPartLines, bookPart);
		}
	}
	
	//adds book part to map, where name of book part is key and value is number of same book parts
	protected void addPartToMap(String key)
	{
		AtomicInteger value;
		if((value = finishedParts.get(key)) == null)
		{
			finishedParts.put(key, new AtomicInteger(1));
		}
		else
		{
			value.incrementAndGet();
		}
	}
	
	//method for underling to save state
	protected synchronized void saveState(String stateName, String path)
	{
		Main.save(path,
				  STATE_FILE,
				  Collections.singletonList(stateName));
	}
	
	//this thread waits until underlings to do their job and then save's their work
	protected void collectWork()
	{
		Map<Integer, BookPart> states;
		
		while(workLeft.get() > 0)
		{
			if((states = underlingWorkQueue.poll()) != null)
			{
				saveWork(states);
			}
			else //underlings are still working but there is no work for this thread
			{
				syncSuperior(false);
			}
		}
		
		//make sure there is no work
		while((states = underlingWorkQueue.poll()) != null)
		{
			saveWork(states);
		}
	}
	
	//underling sends it's states to queue and wakes up superior
	protected void returnStates(Map<Integer, BookPart> currentStates)
	{
		underlingWorkQueue.add(currentStates);
		workLeft.decrementAndGet();
		syncSuperior(true);
	}
	
	//adds work for underlings and wake one of them
	protected void sendWork(OccurrenceMap newBookPart)
	{
		bookPartsQueue.add(newBookPart);
		workLeft.incrementAndGet();
		syncUnderling(0);
	}
	
	private synchronized void syncSuperior(boolean wakeUp)
	{
		if(wakeUp)
		{
			notify();
		}
		else
		{
			try
			{
				wait(TIMEOUT);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	//method for underlings to get work or wait if there is no work or end if all work is done
	protected OccurrenceMap getWork()
	{
		OccurrenceMap bookPart;
		
		while((bookPart = bookPartsQueue.poll()) == null && !workDone.get())
		{
			syncUnderling(-1);
		}
		
		//if volume is null then thread will end
		return bookPart;
	}
	
	//<0 = wait, 0 = notify, >0 = notifyAll
	private synchronized void syncUnderling(int wakeUp)
	{
		if(wakeUp < 0)
		{
			try
			{
				wait(TIMEOUT);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		else if(wakeUp == 0)
		{
			notify();
		}
		else
		{
			notifyAll();
		}
	}
}